set terminal X11
set xlabel "s"
set ylabel "V"
set grid
unset logscale x 
set xrange [0.000000e+00:1.500000e-03]
unset logscale y 
set yrange [-3.710528e-04:3.948311e-04]
#set xtics 1
#set x2tics 1
#set ytics 1
#set y2tics 1
set format y "%g"
set format x "%g"
set key top left
plot 'plot.data' using 3:4 with lines lw 1 title "saida-2.5" ,\
     'plot.data' using 1:(225*$2) with lines lw 1 title "entrada*225" 
set terminal push
set terminal png
set out 'plot.png'
replot
set term pop
replot
