set terminal x11 dashed enhanced   size 600, 500

if(GPVAL_VERSION >= 5.0){set for [i=1:8] linetype i dashtype i}
set size 1.0, 1.0
set origin 0.0, 0.0
set obj 1 rectangle behind from screen 0.0,0.0 to screen 1.0,1.0
set obj 1 fc rgb '#ffffff' fs solid 1.0 noborder 
set size noratio
set xrange [0.8:220.00000000000009]
set yrange [17.479100959511893:66.60355301565818]
set cbrange [*:*]
set logscale x
unset logscale x2
unset logscale y
unset logscale y2
unset logscale cb
unset grid
set title ''
set xlabel 'X'
set x2label ''
set ylabel 'Y'
set y2label ''
set border 15
set key top right
unset xzeroaxis
unset yzeroaxis
unset x2tics
set xtics nomirror
set xtics norotate border autofreq
unset y2tics
set ytics nomirror
set ytics norotate border autofreq
set cbtics autofreq
set colorbox
set cblabel ''
set palette rgbformulae 7,5,15
plot './data5830.gnuplot' index 0 t '' w l lw 1 lt 1 lc rgb '#0000ff' axis x1y1
pause -1
