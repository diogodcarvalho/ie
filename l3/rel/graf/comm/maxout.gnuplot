set terminal png  size 600, 500
set output "../comm.png"
set xrange [0.01:300.0]
set yrange [-20.473159642392815:-0.41353308685189155]
set xlabel "f(Hz)"
set ylabel "Ganho(dB)"
set grid xtics ytics mxtics
set mxtics 10
set logscale x
plot 'data.gnuplot' index 0 t '' w l lw 1 lt 1 lc rgb '#0000ff' axis x1y1, \
	'data.gnuplot' index 1 t '' w p ps 1 pt 1 lc rgb '#ff0000' axis x1y1
