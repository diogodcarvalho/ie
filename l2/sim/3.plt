set terminal X11
set title "ie lab1"
set xlabel "s"
set grid
unset logscale x 
set xrange [0.000000e+00:5.000000e-03]
unset logscale y 
set yrange [-1.110000e-02:1.891000e-01]
#set xtics 1
#set x2tics 1
#set ytics 1
#set y2tics 1
set format y "%g"
set format x "%g"
plot '3.data' using 1:2 with lines lw 1 title "v(10)" ,\
'3.data' using 3:4 with lines lw 1 title "100*v(13)+1.6" 
set terminal push
set terminal png
set out '3.png'
replot
set term pop
replot
